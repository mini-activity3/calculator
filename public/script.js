$(document).ready(()=>{
    

    
    

    $("#addition").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1+value2);
    })
    $("#subtraction").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1-value2);
    })
    $("#multiplication").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1*value2);
    })
    $("#division").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1/value2);
    })
    $("#modulus").click(()=>{
        var value1 = Number($("#first-val").val());
        var value2 = Number($("#second-val").val());
        
        $(".output-box").html(value1%value2);
    })
    $("#clear").click(()=>{
        $("#first-val").val(null)
        $("#second-val").val(null) 
        
        $(".output-box").html("");
    })

})